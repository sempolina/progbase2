#pragma once
#include "dyn_arr.h"
class Deque
{
    DynamicArray array_;
    size_t size_;

public:
    Deque();
    ~Deque();
    size_t size();
    void push_back(float value);
    float pop_back();
    void push_front(float value);
    float pop_front();
    bool empty();
    void print();
};