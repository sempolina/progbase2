#include "dyn_arr.h"
DynamicArray::DynamicArray(size_t initial_cap)
{
    items_ = new float[initial_cap];
    capacity_ = initial_cap;
}
DynamicArray::~DynamicArray()
{
    delete[] items_;
}
size_t DynamicArray::size()
{
    return capacity_;
}
void DynamicArray::set(int index, float value)
{
    items_[index] = value;
}
float DynamicArray::get(int index)
{
    return items_[index];
}
void DynamicArray::resize(size_t newSize)
{
    float *n = new float[newSize];
    int min;
    if (newSize<capacity_) min=newSize;
    else min=capacity_;
    for (int i = 0; i < min; i++)
    {
        n[i] = items_[i];
    }
    delete[] items_;
    items_ = n;
    capacity_ = newSize;
}
