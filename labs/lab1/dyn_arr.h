#pragma once 
#include <cstdlib>
#include <iostream>
class DynamicArray
{
    float *items_;
    int capacity_;

public:
    DynamicArray(size_t size);
    ~DynamicArray();
    size_t size();
    void resize(size_t newSize);
    float get(int index);
    void set(int index, float value);
};