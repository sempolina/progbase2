#pragma once
#include "dyn_arr.h"
class List
{
    DynamicArray array_;
    size_t size_;

public:
    List();
    ~List();
    size_t size();
    float get(int index);
    void set(int index, float value);
    void remove_at(int index);
    void push_back(float value);
    // void push_front(float value);
    bool empty();
    void print();
};