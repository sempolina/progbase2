#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "list.h"
using namespace std;
List::List() : array_{10}
{
    size_ = 0;
}
List::~List()
{
    
}
size_t List::size()
{
    return size_;
}
float List::get(int index)
{
    return array_.get(index);
}
void List::set(int index, float value)
{
    array_.set(index, value);
}
void List::remove_at(int index)
{
    size_--;
    for (int i = index; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
}
void List::push_back(float value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ + 10);
    }
    array_.set(size_, value);
    size_++;
}
// void List::push_front(float value)
// {
//     if (size_ == array_.size())
//     {
//         array_.resize(array_.size + 10);
//     }
//     for (int i = size_; i > 0; i--)
//     {
//         array_.set(i, array_.get(i - 1));
//     }
//     size_++;
//     array_.set(0, value);
// }
bool List::empty()
{
    if (size_ == 0)
        return true;
    else
        return false;
}
void List::print()
{
    for(int i=0;i<size_;i++)
    {
    cout<<array_.get(i)<<" ";
    }
    cout<<endl;
}