#include "list.h"
#include "deque.h"
#include <math.h>
#include <fstream>
using namespace std;
int main()
{
    ifstream fin("data.txt");
    if(!fin)
    {
        fprintf(stderr,"File can`t be open");
        exit(1);
    }
    List l;
    while (!fin.eof())
    {
        float value;
        fin >> value;
        l.push_back(value);
    }
    cout << "File:";
    l.print();
    fin.close();
    for (int i = 0; i < l.size(); i++)
    {
        float value;
        value = l.get(i);
        if (fabs(value) > 10)
        {
            bool k = true;
            for (int j = i; j < l.size(); j++)
            {
                if (fabs(l.get(j)) <= 10)
                {
                    k = false;
                    break;
                }
            }
            if (k == true)
                break;
            l.remove_at(i);
            l.push_back(value);
            i--;
        }
    }
    cout << "Module bigger than 10:";
    l.print();
    Deque deq1;
    Deque deq2;
    for (int i = 0; i < l.size(); i++)
    {
        if (i % 2 == 0)
            deq2.push_back(l.get(i));
        else
            deq1.push_front(l.get(i));
    }
    cout << "Deque1:";
    deq1.print();
    cout << "Deque2:";
    deq2.print();
    List l2;
    while (!deq1.empty())
    {
        l2.push_back(deq1.pop_back());
    }
    while (!deq2.empty())
    {
        l2.push_back(deq2.pop_front());
    }
    cout << "List2:";
    l2.print();
}