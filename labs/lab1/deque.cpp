#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "deque.h"
using namespace std;
Deque::Deque() :array_{10}
{
    size_ = 0;
}
Deque::~Deque()
{
    //array_.~DynamicArray();
}
size_t Deque::size()
{
    return size_;
}
void Deque::push_back(float value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ + 10);
    }
    array_.set(size_, value);
    size_++;
}
void Deque::push_front(float value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ + 10);
    }
    for (int i = size_; i > 0; i--)
    {
        array_.set(i, array_.get(i - 1));
    }
    size_++;
    array_.set(0, value);
}
float Deque::pop_back()
{
    if (size_!=0)
        size_--;
    return array_.get(size_);
}
float Deque::pop_front()
{
    float k=array_.get(0);
    if (size_!=0)
        size_--;
    for (int i = 0; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
    return k;
}
bool Deque::empty()
{
    if (size_ == 0)
        return true;
    else
        return false;
}
void Deque::print()
{
    for(int i=0;i<size_;i++)
    {
    cout<<array_.get(i)<<" ";
    }
    cout<<endl;
}
