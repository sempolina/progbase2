#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#include <iostream>
#include<fstream>
#include <optional>
#include <string>
#include <csvlab.h>
#include <storage.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
using namespace std;

class SqliteStorage:public Storage
{
protected:
    QSqlDatabase db;
public:
    SqliteStorage(const string &dir_name);
    bool isOpen() const;
    bool open();
    void close();
    
    vector<Photo> getAllPhotos();
    optional<Photo> getPhotoById(int photo_id);
    bool updatePhoto(const Photo & photo);
    bool removePhoto(int photo_id);
    int insertPhoto(const Photo & photo);

    vector<Album> getAllAlbums();
    optional<Album> getAlbumById(int album_id);
    bool updateAlbum(const Album & album);
    bool removeAlbum(int album_id);
    int insertAlbum(const Album & album);
};

#endif // SQLITE_STORAGE_H
