#include "csvstorage.h"
using namespace std;
static vector<Photo> createPhotoListFromTable(StringTable &csvTable)
{
    vector<Photo> ls;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        Photo ph;
        ph.id = atoi(csvTable.at(i, 0).c_str());
        ph.User = csvTable.at(i, 1);
        ph.likes = atoi(csvTable.at(i, 2).c_str());
        ph.comments = atoi(csvTable.at(i, 3).c_str());
        ls.push_back(ph);
    }
    return ls;
}
vector<Photo> CsvStorage::loadPhotos()
{
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(photos_file_, str2);
    while (!photos_file_.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(photos_file_, str2);
    }
    str.append(str2);
    str.push_back('\n');
    this->close();
    StringTable inCsvTable = Csv_parse(str);
    vector<Photo> items = createPhotoListFromTable(inCsvTable);
    return items;
}
static StringTable createTableFromPhotoList(vector<Photo> &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) = "id";
    st.at(0, 1) = "User";
    st.at(0, 2) = "Likes";
    st.at(0, 3) = "Comments";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = to_string(ls[i].id);
        st.at(i + 1, 1) = ls[i].User;
        st.at(i + 1, 2) = to_string(ls[i].likes);
        st.at(i + 1, 3) = to_string(ls[i].comments);
    }
    return st;
}
void CsvStorage::savePhotos(const vector<Photo> &photos)
{
    vector<Photo> ph = photos;
    StringTable outCsvTable = createTableFromPhotoList(ph);
    string strout = Csv_toString(outCsvTable);
    photos_file_.open(this->name()+photo_,ios::out);
    if (!photos_file_.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    photos_file_ << strout;
    photos_file_.close();
}
int CsvStorage::getNewPhotoId()
{
    fstream id(this->name()+ "photos_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(this->name() + "photos_id.csv", ios::out);
    if (m == 0)
    {
        vector<Photo> ph = this->loadPhotos();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}
static vector<Album> createAlbumListFromTable(StringTable &csvTable)
{
    vector<Album> ls;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        Album ph;
        ph.id = atoi(csvTable.at(i, 0).c_str());
        ph.name = csvTable.at(i, 1);
        ph.coverPhoto = atoi(csvTable.at(i, 2).c_str());
        ph.n_photos = atoi(csvTable.at(i, 3).c_str());
        ls.push_back(ph);
    }
    return ls;
}
vector<Album> CsvStorage::loadAlbum()
{
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(album_file, str2);
    while (!album_file.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(album_file, str2);
    }
    str.append(str2);
    str.push_back('\n');
    this->close();
    StringTable inCsvTable = Csv_parse(str);
    vector<Album> items = createAlbumListFromTable(inCsvTable);
    return items;
}
static StringTable createTableFromAlbumList(vector<Album> &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) = "id";
    st.at(0, 1) = "Name";
    st.at(0, 2) = "Photo cover";
    st.at(0, 3) = "Number of photos";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = to_string(ls[i].id);
        st.at(i + 1, 1) = ls[i].name;
        st.at(i + 1, 2) = to_string(ls[i].coverPhoto);
        st.at(i + 1, 3) = to_string(ls[i].n_photos);
    }
    return st;
}
void CsvStorage::saveAlbums(const vector<Album> &album)
{
    vector<Album> al = album;
    StringTable outCsvTable = createTableFromAlbumList(al);
    string strout = Csv_toString(outCsvTable);
    album_file.open(this->name()+album_,ios::out);
    if (!album_file.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    album_file << strout;
    album_file.close();
}
int CsvStorage::getNewAlbumId()
{
    fstream id(this->name() + "album_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(this->name() + "album_id.csv", ios::out);
    if (m == 0)
    {
        vector<Album> ph = this->loadAlbum();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}
