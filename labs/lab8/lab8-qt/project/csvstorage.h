#ifndef CSVSTORAGE_H
#define CSVSTORAGE_H
#include "filestorage.h"
using namespace std;
class CsvStorage: public FileStorage
{
    vector<Photo> loadPhotos();
    void savePhotos(const vector<Photo> & photos);
    int getNewPhotoId();

    int getNewAlbumId();
    vector<Album> loadAlbum();
    void saveAlbums(const vector<Album> & album);
    public:
    explicit CsvStorage(const string & dir_name=""):FileStorage(dir_name){photo_="photos.csv";album_="album.csv";}
     ~CsvStorage(){}
};

#endif // CSVSTORAGE_H
