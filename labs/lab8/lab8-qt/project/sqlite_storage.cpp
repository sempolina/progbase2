#include "sqlite_storage.h"
#include <QDebug>
using namespace std;
SqliteStorage::SqliteStorage(const string &dir_name): Storage(dir_name)
{
db =QSqlDatabase ::addDatabase("QSQLITE");
}
bool SqliteStorage::isOpen() const
{
    return db.isOpen();
}
bool SqliteStorage::open()
{
   QString path =QString::fromStdString(this->name())+"data.sqlite";
    db.setDatabaseName(path);
    if(!db.open())
        return false;
     return true;
}
void SqliteStorage::close()
{
    db.close();
}
vector<Photo> SqliteStorage::getAllPhotos()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Photo> photos;
    QSqlQuery query("SELECT * FROM photos");
    if(!query.exec())
    {
        qDebug()<<"get Photos error"<<query.lastError();
        this->close();
        return photos;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string user=query.value("user").toString().toStdString();
           int likes=query.value("likes").toInt();
           int comments=query.value("comments").toInt();
           Photo ph;
           ph.id=id;
           ph.User=user;
           ph.likes=likes;
           ph.comments=comments;
           photos.push_back(ph);
       }
       this->close();
       return photos;
}
optional<Photo> SqliteStorage::getPhotoById(int photo_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM photos WHERE id = :id");
   query.bindValue(":id", photo_id);
   if(!query.exec())
   {
           qDebug()<<"get Photo error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string user=query.value("user").toString().toStdString();
   int likes=query.value("likes").toInt();
   int comments=query.value("comments").toInt();
   Photo ph;
   ph.id=id;
   ph.User=user;
   ph.likes=likes;
   ph.comments=comments;
   this->close();
   return ph;

}
bool SqliteStorage::updatePhoto(const Photo & photo)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE photos SET user = :user, likes = :likes, comments = :comments WHERE id = :id");
   query.bindValue(":id", photo.id);
   query.bindValue(":user", QString::fromStdString(photo.User));
   query.bindValue(":likes", photo.likes);
   query.bindValue(":comments", photo.comments);
   if(!query.exec())
   {
           qDebug()<<"update photo error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removePhoto(int photo_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM photos WHERE id = :id");
 query.bindValue(":id", photo_id);
 if(!query.exec())
 {
         qDebug()<<"delete Photo error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 return true;
}
int SqliteStorage::insertPhoto(const Photo & photo)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO photos (user,likes, comments) VALUES (:user, :likes, :comments)");
   query.bindValue(":user", QString::fromStdString(photo.User));
   query.bindValue(":likes", photo.likes);
   query.bindValue(":comments", photo.comments);
   if(!query.exec())
   {
       qDebug()<<"add Photo error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
vector<Album> SqliteStorage::getAllAlbums()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Album> albums;
    QSqlQuery query("SELECT * FROM albums");
    if(!query.exec())
    {
        qDebug()<<"get Albums error"<<query.lastError();
        this->close();
        return  albums;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string name=query.value("name").toString().toStdString();
           int photo_cover=query.value("photo_cover").toInt();
           int number_photos=query.value("number_of_photos").toInt();
           Album al;
           al.id=id;
           al.name=name;
           al.coverPhoto=photo_cover;
           al.n_photos=number_photos;
           albums.push_back(al);
       }
       this->close();
       return albums;
}
optional<Album> SqliteStorage::getAlbumById(int album_id)
{
    
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM albums WHERE id = :id");
   query.bindValue(":id", album_id);
   if(!query.exec())
   {
           qDebug()<<"get ALbum error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string name=query.value("name").toString().toStdString();
   int photo_cover=query.value("photo_cover").toInt();
   int number_photos=query.value("number_of_photos").toInt();
   Album al;
   al.id=id;
   al.name=name;
   al.coverPhoto=photo_cover;
   al.n_photos=number_photos;
   this->close();
   return al;
}
bool SqliteStorage::updateAlbum(const Album & album)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE albums SET name = :name, photo_cover = :photo_cover, number_of_photos = :number_of_photos WHERE id = :id");
   query.bindValue(":id", album.id);
   query.bindValue(":name", QString::fromStdString(album.name));
   query.bindValue(":photo_cover", album.coverPhoto);
   query.bindValue(":number_of_photos", album.n_photos);
   if(!query.exec())
   {
           qDebug()<<"update Album error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removeAlbum(int album_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM albums WHERE id = :id");
 query.bindValue(":id", album_id);
 if(!query.exec())
 {
         qDebug()<<"delete Album error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 return true;
    
}
int SqliteStorage::insertAlbum(const Album & album)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO albums (name,photo_cover, number_of_photos) VALUES (:name,:photo_cover,:number_of_photos)");
   query.bindValue(":name", QString::fromStdString(album.name));
   query.bindValue(":photo_cover", album.coverPhoto);
   query.bindValue(":number_of_photos", album.n_photos);
   if(!query.exec())
   {
       qDebug()<<"add Album error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
