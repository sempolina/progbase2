#ifndef STORAGE_H
#define STORAGE_H

#include<vector>
#include "photo.h"
#include "album.h"
#include <optional>
using namespace  std;

class Storage
{
private:
    string dir_name_;
public:
    Storage(){}
    explicit Storage(const string & dir_name=""){dir_name_ = dir_name;}
    virtual ~Storage(){}
    void setName(const string & dir_name);
    string name() const;

     virtual bool isOpen() const=0;
    virtual bool open()=0;
     virtual void close()=0;

     virtual vector<Photo> getAllPhotos()=0;
     virtual optional<Photo> getPhotoById(int photo_id)=0;
     virtual bool updatePhoto(const Photo & photo)=0;
     virtual bool removePhoto(int photo_id)=0;
     virtual int insertPhoto(const Photo & photo)=0;

     virtual vector<Album> getAllAlbums()=0;
     virtual optional<Album> getAlbumById(int album_id)=0;
     virtual bool updateAlbum(const Album & album)=0;
     virtual bool removeAlbum(int album_id)=0;
     virtual int insertAlbum(const Album & album)=0;
};

#endif // STORAGE_H
