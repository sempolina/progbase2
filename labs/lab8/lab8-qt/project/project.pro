QT +=xml
QT +=sql
QT -= gui

CONFIG += c++1z console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    filestorage.cpp \
    cui.cpp \
    csvstorage.cpp \
    xmlstorage.cpp \
    storage.cpp \
    sqlite_storage.cpp

HEADERS += \
    filestorage.h \
    cui.h \
    photo.h \
    album.h \
    csvstorage.h \
    xmlstorage.h \
    storage.h \
    sqlite_storage.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../csvlab/release/ -lcsvlab
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../csvlab/debug/ -lcsvlab
else:unix: LIBS += -L$$OUT_PWD/../csvlab/ -lcsvlab

INCLUDEPATH += $$PWD/../csvlab
DEPENDPATH += $$PWD/../csvlab

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/release/libcsvlab.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/debug/libcsvlab.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/release/csvlab.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/debug/csvlab.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../csvlab/libcsvlab.a



win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../csvlab/release/ -lcsvlab
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../csvlab/debug/ -lcsvlab
else:unix: LIBS += -L$$OUT_PWD/../csvlab/ -lcsvlab

INCLUDEPATH += $$PWD/../csvlab
DEPENDPATH += $$PWD/../csvlab

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/release/libcsvlab.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/debug/libcsvlab.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/release/csvlab.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../csvlab/debug/csvlab.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../csvlab/libcsvlab.a
