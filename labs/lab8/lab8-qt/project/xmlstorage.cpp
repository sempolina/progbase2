#include "xmlstorage.h"
using namespace std;
static Photo xmlElementToPhoto(QDomElement & photoE1)
{
    Photo photo;
    photo.id=photoE1.attribute("id").toInt();
    photo.User=photoE1.attribute("User").toStdString();
    photo.likes=photoE1.attribute("Likes").toInt();
    photo.comments=photoE1.attribute("Comments").toInt();
    return photo;
}
vector<Photo> XmlStorage::loadPhotos()
{
    vector<Photo> ph;
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(photos_file_, str2);
    while (!photos_file_.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(photos_file_, str2);
    }
    str.append(str2);
    this->close();
    QDomDocument doc;
    QString errorMessage;
    QString xmlstr=QString::fromStdString(str);
    if(!doc.setContent(xmlstr,&errorMessage))
    {
        qDebug()<<"error parsing xml: "<<errorMessage;
        exit(1);
    }
    QDomElement rootE1=doc.documentElement();
    QDomNodeList rootChildren=rootE1.childNodes();
    for(int i=0;i<rootChildren.length();i++)
    {
        QDomNode photoNode=rootChildren.at(i);
        QDomElement photoE1=photoNode.toElement();
        Photo photo = xmlElementToPhoto(photoE1);
        ph.push_back(photo);
    }
    return ph;
}
static QDomElement photoToXmlElement(QDomDocument & doc,const Photo & photo)
{
 QDomElement photoE1=doc.createElement("Photo");
 photoE1.setAttribute("id",photo.id);
 photoE1.setAttribute("User",QString::fromStdString(photo.User));
 photoE1.setAttribute("Likes",photo.likes);
 photoE1.setAttribute("Comments",photo.comments);
 return photoE1;
}
void XmlStorage::savePhotos(const vector<Photo> & photos)
{
    QDomDocument doc;
    QDomElement rootEl =doc.createElement("Photos");
    for(int i=0;i<photos.size();i++)
    {
        QDomElement photoE1=photoToXmlElement(doc,photos[i]);
        rootEl.appendChild(photoE1);
    }
    doc.appendChild(rootEl);
    QString qStr =doc.toString(4);
    photos_file_.open(this->name()+photo_,ios::out);
    if (!photos_file_.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    photos_file_ << qStr.toStdString();
    photos_file_.close();
}
int XmlStorage::getNewPhotoId()
{
    fstream id(this->name()+ "photos_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(this->name() + "photos_id.csv", ios::out);
    if (m == 0)
    {
        vector<Photo> ph = this->loadPhotos();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}
static Album xmlElementToAlbum(QDomElement & albumE1)
{
    Album album;
    album.id=albumE1.attribute("id").toInt();
    album.name=albumE1.attribute("Name").toStdString();
    album.coverPhoto=albumE1.attribute("Photo_cover").toInt();
    album.n_photos=albumE1.attribute("Number_of_photos").toInt();
    return album;
}
vector<Album> XmlStorage::loadAlbum()
{
    vector<Album> al;
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(album_file, str2);
    while (!album_file.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(album_file, str2);
    }
    str.append(str2);
    this->close();
    QDomDocument doc;
    QString errorMessage;
    QString xmlstr=QString::fromStdString(str);
    if(!doc.setContent(xmlstr,&errorMessage))
    {
        qDebug()<<"error parsing xml: "<<errorMessage;
        exit(1);
    }
    QDomElement rootE1=doc.documentElement();
    QDomNodeList rootChildren=rootE1.childNodes();
    for(int i=0;i<rootChildren.length();i++)
    {
        QDomNode albumNode=rootChildren.at(i);
        QDomElement albumE1=albumNode.toElement();
        Album album = xmlElementToAlbum(albumE1);
        al.push_back(album);
    }
    return al;
}
static QDomElement albumToXmlElement(QDomDocument & doc,const Album & album)
{
 QDomElement albumE1=doc.createElement("Album");
 albumE1.setAttribute("id",album.id);
 albumE1.setAttribute("Name",QString::fromStdString(album.name));
 albumE1.setAttribute("Photo_cover",album.coverPhoto);
 albumE1.setAttribute("Number_of_photos",album.n_photos);
 return albumE1;
}
void XmlStorage::saveAlbums(const vector<Album> & album)
{
    QDomDocument doc;
    QDomElement rootEl =doc.createElement("Albums");
    for(int i=0;i<album.size();i++)
    {
        QDomElement albumE1=albumToXmlElement(doc,album[i]);
        rootEl.appendChild(albumE1);
    }
    doc.appendChild(rootEl);
    QString qStr =doc.toString(4);
    album_file.open(this->name()+album_,ios::out);
    if (!album_file.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    album_file << qStr.toStdString();
    album_file.close();
}
int XmlStorage::getNewAlbumId()
{
    fstream id(this->name() + "album_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(this->name() + "album_id.csv", ios::out);
    if (m == 0)
    {
        vector<Album> ph = this->loadAlbum();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}
