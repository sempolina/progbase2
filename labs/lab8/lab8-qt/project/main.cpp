#include <string>
#include <vector>
#include "cui.h"
#include "csvstorage.h"
#include"xmlstorage.h"
#include"sqlite_storage.h"
using namespace std;

int main()
{
    cout<<"Please, enter name of directory"<<endl;
    string str;
    cin>>str;
    cout<<"Please, choose data format:\n1. CSV\n2. XML\n3. Sqlite"<<endl;
    int com=0;
    cin>>com;
    while(com!=1&&com!=2&&com!=3)
    {
        cout<<"Incorrect command. Please, try again"<<endl;
        cin>>com;
    }
    if(com==1)
    {
        CsvStorage csv_storage(str);
        Storage * storage_ptr=&csv_storage;
        Cui cui(storage_ptr);
        cui.show();
    }
    else if(com==2)
    {
        XmlStorage xml_storage(str);
        Storage * storage_ptr=&xml_storage;
        Cui cui(storage_ptr);
        cui.show();
    }
    else
    {
        SqliteStorage sq_storage(str);
        Storage * storage_ptr=&sq_storage;
        Cui cui(storage_ptr);
        cui.show();
    }
}
