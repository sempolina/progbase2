#ifndef FILESTORAGE_H
#define FILESTORAGE_H


#include <iostream>
#include<fstream>
#include<vector>
#include <optional>
#include "photo.h"
#include "album.h"
#include <string>
#include <csvlab.h>
using namespace std;

class FileStorage
{
    string dir_name_;
protected:
    fstream photos_file_;
    fstream album_file;
    string photo_="";
    string album_="";
    virtual vector<Photo> loadPhotos()=0;
    virtual void savePhotos(const vector<Photo> & photos)=0;
    virtual int getNewPhotoId()=0;

    virtual int getNewAlbumId()=0;
    virtual vector<Album> loadAlbum()=0;
    virtual void saveAlbums(const vector<Album> & album)=0;
    public:
    explicit FileStorage(const string & dir_name=""){dir_name_ = dir_name;}
    virtual ~FileStorage(){}
     void setName(const string & dir_name);
     string name();
     bool isOpen();
    bool open();
     void close();

     vector<Photo> getAllPhotos();
     optional<Photo> getPhotoById(int photo_id);
     bool updatePhoto(const Photo & photo);
     bool removePhoto(int photo_id);
     int insertPhoto(const Photo & photo);

     vector<Album> getAllAlbums();
     optional<Album> getAlbumById(int album_id);
     bool updateAlbum(const Album & album);
     bool removeAlbum(int album_id);
     int insertAlbum(const Album & album);
};

#endif // FILESTORAGE_H
