#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H
#include "filestorage.h"
#include <QtXml>
#include <QDebug>
class XmlStorage: public FileStorage
{
protected:
    void savePhotos(const vector<Photo> & photos);
    vector<Photo> loadPhotos();
    int getNewPhotoId();

    int getNewAlbumId();
    vector<Album> loadAlbum();
    void saveAlbums(const vector<Album> & album);
    public:
    explicit XmlStorage(const string & dir_name_=""):FileStorage(dir_name_){photo_="photos.xml";album_="album.xml";}
};

#endif // XMLSTORAGE_H
