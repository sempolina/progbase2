#include <string>
#include <vector>
#include "cui.h"
#include "csvstorage.h"
#include"xmlstorage.h"
using namespace std;

int main()
{
    cout<<"Please, enter name of directory"<<endl;
    string str;
    cin>>str;
    cout<<"Please, choose data format:\n1. CSV\n2. XML"<<endl;
    int com=0;
    cin>>com;
    while(com!=1&&com!=2)
    {
        cout<<"Incorrect command. Please, try again"<<endl;
        cin>>com;
    }
    if(com==1)
    {
        CsvStorage csv_storage(str);
        FileStorage * storage_ptr=&csv_storage;
        Cui cui(storage_ptr);
        cui.show();
    }
    else
    {
        XmlStorage xml_storage(str);
        FileStorage * storage_ptr=&xml_storage;
        Cui cui(storage_ptr);
        cui.show();
    }
}
