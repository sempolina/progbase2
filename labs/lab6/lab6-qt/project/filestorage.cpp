#include "filestorage.h"

using namespace std;

bool FileStorage::open()
{

//            album_file.open(this->name() + "album.csv", ios::out);

//            photos_file_.open(this->name() + "photos.csv", ios::out);

        photos_file_.open(dir_name_+photo_);
        album_file.open(dir_name_+album_);
    if(photos_file_.is_open()==true||album_file.is_open()==true)
        return true;
    else
        return false;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name()
{
    return dir_name_;
}
bool FileStorage::isOpen()
{
    if(photos_file_.is_open()==true&&album_file.is_open()==true)
        return true;
    else
        return false;
}
void FileStorage::close()
{
    photos_file_.close();
    album_file.close();
}
vector<Photo> FileStorage::getAllPhotos()
{
    return this->loadPhotos();
}
optional<Photo> FileStorage::getPhotoById(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo_id)
            return photos[i];
    }
    return nullopt;
}
bool FileStorage::updatePhoto(const Photo &photo)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo.id)
        {
            photos[i] = photo;
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
bool FileStorage::removePhoto(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    auto it = photos.begin();
    auto it_end = photos.end();
    for (; it != it_end; it++)
    {
        if ((*it).id == photo_id)
        {
            photos.erase(it);
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
int FileStorage::insertPhoto(const Photo &photo)
{
    int newId = this->getNewPhotoId();
    Photo copy = photo;
    copy.id = newId;
    vector<Photo> photos = this->loadPhotos();
    photos.push_back(copy);
    this->savePhotos(photos);
    return newId;
}
vector<Album> FileStorage::getAllAlbums()
{
     return this->loadAlbum();
}
optional<Album> FileStorage::getAlbumById(int album_id)
{
    vector<Album> album = this->loadAlbum();
    for (int i = 0; i < album.size(); i++)
    {
        if (album[i].id == album_id)
            return album[i];
    }
    return nullopt;
}
bool FileStorage::updateAlbum(const Album &album)
{
     vector<Album> albums = this->loadAlbum();
    for (int i = 0; i < albums.size(); i++)
    {
        if (albums[i].id == album.id)
        {
            albums[i] = album;
            this->saveAlbums(albums);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeAlbum(int album_id)
{
    vector<Album> album = this->loadAlbum();
    auto it = album.begin();
    auto it_end = album.end();
    for (; it != it_end; it++)
    {
        if ((*it).id == album_id)
        {
            album.erase(it);
            this->saveAlbums(album);
            return true;
        }
    }
    return false;
}
int FileStorage::insertAlbum(const Album &album)
{
    int newId = this->getNewAlbumId();
    Album copy = album;
    copy.id = newId;
    vector<Album> albums = this->loadAlbum();
    albums.push_back(copy);
    this->saveAlbums(albums);
    return newId;
}
