#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "adddialog.h"
#include "edditdialog.h"
#include <QMessageBox>
#include "sqlite_storage.h"
#include "adddialog2.h"
#include "auth.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pix("../camera.png");
    ui->label->setPixmap(pix);
    connect(ui->actionOpen,&QAction::triggered,this,&MainWindow::onOpen);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::beforeClose);
    connect(ui->actionlogout,&QAction::triggered,this,&MainWindow::onLogout);
}

MainWindow::~MainWindow()
{
    if(storage_!=nullptr)
        delete storage_;
    delete ui;
}
void MainWindow::addPhotos(const vector<Photo> & photos)
{
    for(const Photo & photo:photos)
    {
        QVariant qVar=QVariant::fromValue(photo);
        QListWidgetItem * qPhotoListItem=new QListWidgetItem();
        qPhotoListItem->setText(QString::fromStdString(photo.User));
        qPhotoListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qPhotoListItem);
    }
}
void MainWindow::onOpen()
{
   QString fileName=QFileDialog::getOpenFileName(this,"Dialog Caption","","SQLITE (*.sqlite)");
   if(!fileName.isEmpty()&&!fileName.isNull())
   {
       if(storage_!=nullptr)
       {
           delete storage_;
           ui->listWidget->clear();
           ui->listWidget_2->clear();
       }
       storage_=new SqliteStorage(fileName.toStdString());
       ui->label_2->setText("");
       ui->selected->setText("");
       ui->selected_2->setText("");
       ui->remove_button->setEnabled(false);
       ui->edit_button->setEnabled(false);
       ui->remove_button_2->setEnabled(false);
       ui->add_button_2->setEnabled(false);
       ui->add_button->setEnabled(false);
        User user;
       Auth auth(this);
       if(auth.exec())
      {
           user=auth.data();
       }
       else
       {
          return;
       }
       qDebug()<<user.username<<user.hash_password;
       const User u=user;
       while (storage_->getUserAuth(user.username,user.hash_password)==nullopt)
       {
           qDebug()<<user.username;
           QMessageBox::information(this,"Authentication","Incorrect login or password. Try again");
           Auth auth2(this);
           if(auth2.exec())
           {
               user=auth2.data();
           }
           else
           {
               break;
           }
       }
       user_=storage_->getUserAuth(user.username,user.hash_password).value();
       ui->add_button->setEnabled(true);
       ui->label_2->setText(user_.fullname);
       qDebug()<<fileName;
       vector<Photo> photos=storage_->getAllUserPhotos(user_.id);
       this->addPhotos(photos);
   }
   else
   {

   }
}

void MainWindow::addAlbums(const vector<Album> & albums)
{
    for(const Album & album:albums)
    {
        QVariant qVar=QVariant::fromValue(album);
        QListWidgetItem * qALbumListItem=new QListWidgetItem();
        qALbumListItem->setText(QString::fromStdString(album.name));
        qALbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qALbumListItem);
    }
}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
        ui->listWidget_2->clear();
        ui->selected_2->setText("");
        ui->remove_button->setEnabled(true);
        ui->edit_button->setEnabled(true);
          ui->remove_button_2->setEnabled(false);
          ui->add_button_2->setEnabled(true);
    Photo ph= item->data(Qt::UserRole).value<Photo>();
    vector<Album> albums=storage_->getAllPhotoAlbums(ph.id);
    this->addAlbums(albums);
    ui->selected->setText("Selected Photo:\nId:"+QString::number(ph.id)+
                          "\nUser:"+ QString::fromStdString(ph.User)+
                          "\nlikes:"+QString::number(ph.likes)+"\ncomments:"+QString::number(ph.comments));
}

void MainWindow::on_remove_button_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        if(ui->listWidget->selectedItems().isEmpty())
        {
            ui->add_button_2->setEnabled(false);
            ui->selected->setText("");
           ui->remove_button->setEnabled(false);
           ui->edit_button->setEnabled(false);
        }
        ui->selected_2->setText("");
        QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
        Photo ph;
        foreach(QListWidgetItem *item,items)
        {
            ph=item->data(Qt::UserRole).value<Photo>();
            storage_->removePhoto(ph.id);
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        }
        items=ui->listWidget->selectedItems();
        if(items.size()!=0)
        {
            ph=items[0]->data(Qt::UserRole).value<Photo>();
            ui->listWidget_2->clear();
            ui->remove_button_2->setEnabled(false);
            vector<Album> albums=storage_->getAllPhotoAlbums(ph.id);
            this->addAlbums(albums);
        }
        else
        {
            ui->listWidget_2->clear();
            ui->label_2->setText("");
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
        }

    }
}

void MainWindow::on_add_button_clicked()
{

    AddDialog add(this);
    int status=add.exec();
    if(status==1)
    {
        Photo photo=add.data();
        photo.user_id=user_.id;
        photo.id=storage_->insertPhoto(photo);
        QVariant qVar=QVariant::fromValue(photo);
        QListWidgetItem * qPhotoListItem=new QListWidgetItem();
        qPhotoListItem->setText(QString::fromStdString(photo.User));
        qPhotoListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qPhotoListItem);
    }
    else
    {

    }
}

void MainWindow::on_edit_button_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Photo ph;
        foreach(QListWidgetItem *item,items)
        {
            ph=item->data(Qt::UserRole).value<Photo>();
            EdditDialog edit(this);
            edit.setData(ph);
            int status=edit.exec();
            if(status==1)
            {
                Photo photo=edit.data();
                photo.id=ph.id;
                storage_->updatePhoto(photo);
                QVariant qVar=QVariant::fromValue(photo);
                item->setData(Qt::UserRole,qVar);
                item->setText(QString::fromStdString(photo.User));
                ui->listWidget->editItem(item);
                ui->selected->setText("Selected Photo:\nId:"+QString::number(photo.id)+
                                      "\nUser:"+ QString::fromStdString(photo.User)+
                                      "\nlikes:"+QString::number(photo.likes)+"\ncomments:"+QString::number(photo.comments));
            }

        }
}
void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On exit","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        this->close();
    }
}

void MainWindow::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    ui->remove_button_2->setEnabled(true);
Album al= item->data(Qt::UserRole).value<Album>();
ui->selected_2->setText("Selected Albums:\nId:"+QString::number(al.id)+
                      "\nUser:"+ QString::fromStdString(al.name)+
                      "\nlikes:"+QString::number(al.coverPhoto)+"\ncomments:"+QString::number(al.n_photos));
}

void MainWindow::on_add_button_2_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Photo ph;
    foreach(QListWidgetItem *item,items)
    {
        ph=item->data(Qt::UserRole).value<Photo>();
     }
    vector<Album> albums;
    vector<Album> all_al=storage_->getAllAlbums();
    vector<Album> ph_al=storage_->getAllPhotoAlbums(ph.id);
    for(int i=0;i<all_al.size();i++)
    {
        bool exist=false;
        for(int j=0;j<ph_al.size();j++)
        {
            if(all_al[i].id==ph_al[j].id)
            {
                exist=true;
                break;
            }
        }
        if(!exist)
        {
            albums.push_back(all_al[i]);
        }
    }
    adddialog2 add(this);
    add.setData(albums);
    int status=add.exec();
    if(status==1)
    {
        Album album=add.data();
        storage_->insertPhotoAlbum(ph.id,album.id);
        QVariant qVar=QVariant::fromValue(album);
        QListWidgetItem * qAlbumListItem=new QListWidgetItem();
        qAlbumListItem->setText(QString::fromStdString(album.name));
        qAlbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qAlbumListItem);
    }
    else
    {

    }
}

void MainWindow::on_remove_button_2_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {

        QList <QListWidgetItem*> items=ui->listWidget_2->selectedItems();
        Album al;
        al=items[0]->data(Qt::UserRole).value<Album>();
        QList <QListWidgetItem*> items2=ui->listWidget->selectedItems();
        Photo ph=items2[0]->data(Qt::UserRole).value<Photo>();
        qDebug()<<al.id<<"|"<<ph.id;
        try
        {storage_->removePhotoAlbum(ph.id,al.id);}
        catch(string i)
        {
            cout<<i;
        }
        catch(QSqlError error)
        {
            qDebug()<<error.text();
        }
        delete ui->listWidget_2->takeItem(ui->listWidget_2->row(items[0]));
        if(ui->listWidget_2->selectedItems().isEmpty())
        {
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
            ui->selected_2->setText("");
        }
    }
}
void MainWindow::onLogout()
{
 user_.id=0;
 user_.fullname="";
 user_.hash_password="";
 user_.username="";
 ui->label_2->setText("");
 ui->selected->setText("");
 ui->selected_2->setText("");
 ui->remove_button->setEnabled(false);
 ui->edit_button->setEnabled(false);
 ui->remove_button_2->setEnabled(false);
 ui->add_button_2->setEnabled(false);
 ui->add_button->setEnabled(false);
 ui->listWidget->clear();
 ui->listWidget_2->clear();

}
