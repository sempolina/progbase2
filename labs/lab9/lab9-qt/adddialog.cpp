#include "adddialog.h"
#include "ui_adddialog.h"
using namespace std;
AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

Photo AddDialog::data()
{
    Photo ph;
    ph.User=ui->lineEdit->text().toStdString();
    ph.likes=ui->spinBox->value();
    ph.comments=ui->spinBox_2->value();
    return ph;
}
AddDialog::~AddDialog()
{
    delete ui;
}
