#ifndef ALBUM_H
#define ALBUM_H

#include<string>
#include <iostream>
using namespace std;
struct Album
{
    int id;
    string name;
    int coverPhoto;
    int n_photos;
    Album(){}
    Album(const Album & al){this->id=al.id;this->name=al.name;this->coverPhoto =al.coverPhoto;this->n_photos=al.n_photos;}
};
Q_DECLARE_METATYPE(Album)
#endif // ALBUM_H
