#ifndef ADDDIALOG2_H
#define ADDDIALOG2_H

#include <QDialog>
#include "album.h"
#include <QListWidgetItem>
namespace Ui {
class adddialog2;
}

class adddialog2 : public QDialog
{
    Q_OBJECT

public:
    explicit adddialog2(QWidget *parent = 0);
    ~adddialog2();
    Album data();
    void setData(vector<Album> al);
private slots:
    void on_listWidget_itemClicked(QListWidgetItem *item);

private:
    Ui::adddialog2 *ui;
};

#endif // ADDDIALOG2_H
