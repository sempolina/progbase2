#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#include <iostream>
#include<fstream>
#include <optional>
#include <string>
#include <storage.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "user.h"
using namespace std;

class SqliteStorage:public Storage
{
protected:
    QSqlDatabase db;
public:
    //User user;
    SqliteStorage(const string &dir_name);
    bool isOpen() const;
    bool open();
    void close();
    
    vector<Photo> getAllPhotos();
    optional<Photo> getPhotoById(int photo_id);
    bool updatePhoto(const Photo & photo);
    bool removePhoto(int photo_id);
    int insertPhoto(const Photo & photo);

    vector<Album> getAllAlbums();
    optional<Album> getAlbumById(int album_id);
    bool updateAlbum(const Album & album);
    bool removeAlbum(int album_id);     
    int insertAlbum(const Album & album);
    
    optional<User> getUserAuth(QString const & username, QString const & password);
    vector<Photo> getAllUserPhotos(int user_id);

    vector<Album> getAllPhotoAlbums(int photo_id);
    bool insertPhotoAlbum(int photo_id,int album_id);
    bool removePhotoAlbum(int photo_id,int album_id);
};

#endif // SQLITE_STORAGE_H
