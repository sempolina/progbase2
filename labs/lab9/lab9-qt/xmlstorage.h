#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H
#include "filestorage.h"
#include <QtXml>
#include <QDebug>
class XmlStorage: public FileStorage
{
protected:
    void savePhotos(const vector<Photo> & photos);
    vector<Photo> loadPhotos();
    int getNewPhotoId();

    public:
   explicit XmlStorage(const string & dir_name_=""):FileStorage(dir_name_){photo_="photos.xml";}
    ~XmlStorage(){}

};
//Q_DECLARE_METATYPE(XmlStorage);
#endif // XMLSTORAGE_H
