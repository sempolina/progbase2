#ifndef FILESTORAGE_H
#define FILESTORAGE_H


#include <iostream>
#include<fstream>
#include <string>
#include <storage.h>
using namespace std;

class FileStorage : public Storage
{

protected:
    fstream photos_file_;
    fstream album_file;
    string photo_="";
    string album_="";
    virtual vector<Photo> loadPhotos()=0;
    virtual void savePhotos(const vector<Photo> & photos)=0;
    virtual int getNewPhotoId()=0;

    virtual int getNewAlbumId()=0;
    virtual vector<Album> loadAlbum()=0;
    virtual void saveAlbums(const vector<Album> & album)=0;
    public:
    explicit FileStorage(const string & dir_name): Storage(dir_name){}

    virtual ~FileStorage(){}

     bool isOpen() const;
    bool open();
     void close();

     vector<Photo> getAllPhotos();
     optional<Photo> getPhotoById(int photo_id);
     bool updatePhoto(const Photo & photo);
     bool removePhoto(int photo_id);
     int insertPhoto(const Photo & photo);

     vector<Album> getAllAlbums();
     optional<Album> getAlbumById(int album_id);
     bool updateAlbum(const Album & album);
     bool removeAlbum(int album_id);
     int insertAlbum(const Album & album);
};

#endif // FILESTORAGE_H
