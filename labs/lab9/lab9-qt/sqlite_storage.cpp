#include "sqlite_storage.h"
#include <QDebug>
#include <QCryptographicHash>
using namespace std;
SqliteStorage::SqliteStorage(const string &dir_name): Storage(dir_name)
{
db =QSqlDatabase ::addDatabase("QSQLITE");
}
bool SqliteStorage::isOpen() const
{
    return db.isOpen();
}
bool SqliteStorage::open()
{
   QString path =QString::fromStdString(this->name());
    db.setDatabaseName(path);
    if(!db.open())
        return false;
     return true;
}
void SqliteStorage::close()
{
    db.close();
}
vector<Photo> SqliteStorage::getAllPhotos()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Photo> photos;
    QSqlQuery query("SELECT * FROM photos");
    if(!query.exec())
    {
        qDebug()<<"get Photos error"<<query.lastError();
        this->close();
        return photos;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string user=query.value("user").toString().toStdString();
           int likes=query.value("likes").toInt();
           int comments=query.value("comments").toInt();
           int user_id=query.value("user_id").toInt();
           Photo ph;
           ph.id=id;
           ph.User=user;
           ph.likes=likes;
           ph.comments=comments;
           ph.user_id=user_id;
           photos.push_back(ph);
       }
       this->close();
       return photos;
}
optional<Photo> SqliteStorage::getPhotoById(int photo_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM photos WHERE id = :id");
   query.bindValue(":id", photo_id);
   if(!query.exec())
   {
           qDebug()<<"get Photo error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string user=query.value("user").toString().toStdString();
   int likes=query.value("likes").toInt();
   int comments=query.value("comments").toInt();
   int user_id=query.value("user_id").toInt();
   Photo ph;
   ph.id=id;
   ph.User=user;
   ph.likes=likes;
   ph.comments=comments;
   ph.user_id=user_id;
   this->close();
   return ph;

}
bool SqliteStorage::updatePhoto(const Photo & photo)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE photos SET user = :user, likes = :likes, comments = :comments,user_id=:user_id WHERE id = :id");
   query.bindValue(":id", photo.id);
   query.bindValue(":user", QString::fromStdString(photo.User));
   query.bindValue(":likes", photo.likes);
   query.bindValue(":comments", photo.comments);
   query.bindValue(":user_id",photo.user_id);
   if(!query.exec())
   {
           qDebug()<<"update photo error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removePhoto(int photo_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM photos WHERE id = :id");
 query.bindValue(":id", photo_id);
 if(!query.exec())
 {
         qDebug()<<"delete Photo error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 if(!this->open())
 {
     qDebug()<<"Database can`t be open";
     exit(1);
 }
 QSqlQuery query2;
 query2.prepare("DELETE FROM links WHERE id_photos = :id");
 query2.bindValue(":id", photo_id);
 if(!query2.exec())
 {
         qDebug()<<"delete link error"<<query2.lastError();
         this->close();
         return false;
 }
 this->close();
 qDebug()<<"Hello4";
 return true;
}
int SqliteStorage::insertPhoto(const Photo & photo)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO photos (user,likes, comments,user_id) VALUES (:user, :likes, :comments, :user_id)");
   query.bindValue(":user", QString::fromStdString(photo.User));
   query.bindValue(":likes", photo.likes);
   query.bindValue(":comments", photo.comments);
   query.bindValue(":user_id", photo.user_id);
   if(!query.exec())
   {
       qDebug()<<"add Photo error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
vector<Album> SqliteStorage::getAllAlbums()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Album> albums;
    QSqlQuery query("SELECT * FROM albums");
    if(!query.exec())
    {
        qDebug()<<"get Albums error"<<query.lastError();
        this->close();
        return  albums;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string name=query.value("name").toString().toStdString();
           int photo_cover=query.value("photo_cover").toInt();
           int number_photos=query.value("number_of_photos").toInt();
           Album al;
           al.id=id;
           al.name=name;
           al.coverPhoto=photo_cover;
           al.n_photos=number_photos;
           albums.push_back(al);
       }
       this->close();
       return albums;
}
optional<Album> SqliteStorage::getAlbumById(int album_id)
{
    
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM albums WHERE id = :id");
   query.bindValue(":id", album_id);
   if(!query.exec())
   {
           qDebug()<<"get ALbum error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string name=query.value("name").toString().toStdString();
   int photo_cover=query.value("photo_cover").toInt();
   int number_photos=query.value("number_of_photos").toInt();
   Album al;
   al.id=id;
   al.name=name;
   al.coverPhoto=photo_cover;
   al.n_photos=number_photos;
   this->close();
   return al;
}
bool SqliteStorage::updateAlbum(const Album & album)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE albums SET name = :name, photo_cover = :photo_cover, number_of_photos = :number_of_photos WHERE id = :id");
   query.bindValue(":id", album.id);
   query.bindValue(":name", QString::fromStdString(album.name));
   query.bindValue(":photo_cover", album.coverPhoto);
   query.bindValue(":number_of_photos", album.n_photos);
   if(!query.exec())
   {
           qDebug()<<"update Album error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removeAlbum(int album_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM albums WHERE id = :id");
 query.bindValue(":id", album_id);
 if(!query.exec())
 {
         qDebug()<<"delete Album error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 return true;
    
}
int SqliteStorage::insertAlbum(const Album & album)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO albums (name,photo_cover, number_of_photos) VALUES (:name,:photo_cover,:number_of_photos)");
   query.bindValue(":name", QString::fromStdString(album.name));
   query.bindValue(":photo_cover", album.coverPhoto);
   query.bindValue(":number_of_photos", album.n_photos);
   if(!query.exec())
   {
       qDebug()<<"add Album error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
static QString hashPassword(QString const & pass)
{
    QByteArray pass_ba=pass.toUtf8();
    pass_ba=QCryptographicHash::hash(pass_ba,QCryptographicHash::Md5);
    QString pass_hash=QString(pass_ba.toHex());
    return pass_hash;
}
optional<User> SqliteStorage::getUserAuth( QString const & username, QString const & password)
{
    qDebug()<<"Hello";
    if(!this->open())
    {

        throw "Database can`t be open";
     }
  QSqlQuery query;
  if(!query.prepare("SELECT * FROM users WHERE username=:username AND password_hash=:password_hash;"))
  {
      QSqlError error=query.lastError();
      throw error;
  }
  query.bindValue(":username",username);
  query.bindValue(":password_hash",hashPassword(password));
  if(!query.exec())
  {
     QSqlError error =query.lastError();
     throw error;
  }
  if(query.next())
  {
      User user;
      user.id =query.value("id").toInt();
      user.username=username;
      user.hash_password=query.value("password_hash").toString();
      user.fullname=query.value("fullname").toString();
      this->close();
      return user;
  }
  this->close();
  return nullopt;
}
vector<Photo> SqliteStorage::getAllUserPhotos(int user_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    if(!query.prepare("SELECT * FROM photos WHERE user_id=:user_id"))
    {
        QSqlError error=query.lastError();
        throw error;
    }
    query.bindValue(":user_id",user_id);
    if(!query.exec())
    {
       QSqlError error =query.lastError();
       throw error;
    }
    vector<Photo> photos;
    while(query.next())
    {
        Photo p;
        p.id=query.value(0).toInt();
        p.User=query.value(1).toString().toStdString();
        p.likes=query.value(2).toInt();
        p.comments=query.value(3).toInt();
        p.user_id=query.value(4).toInt();
            qDebug()<<p.user_id;
        photos.push_back(p);
    }
    this->close();
    return photos;

}
vector<Album> SqliteStorage::getAllPhotoAlbums(int photo_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    if(!query.prepare("SELECT * FROM links WHERE id_photos=:id_photos"))
    {
        QSqlError error=query.lastError();
        throw error;
    }
    query.bindValue(":id_photos",photo_id);
    if(!query.exec())
    {
       QSqlError error =query.lastError();
       throw error;
    }
    vector<int> id;
    while(query.next())
    {
        int i;
        i=query.value(2).toInt();
        id.push_back(i);
    }
    vector<Album> albums;
    for(int i=0;i<id.size();i++)
    {

        if(!query.prepare("SELECT * FROM albums  WHERE id=:id"))
        {
            QSqlError error=query.lastError();
            throw error;
        }
        query.bindValue(":id",id[i]);
        if(!query.exec())
        {
           QSqlError error =query.lastError();
           throw error;
        }
        if(query.next())
        {
            Album a;
            a.id=query.value(0).toInt();
            a.name=query.value(1).toString().toStdString();
            a.coverPhoto=query.value(2).toInt();
            a.n_photos=query.value(3).toInt();
            albums.push_back(a);
        }
    }
    this->close();
    return albums;
}
bool SqliteStorage::insertPhotoAlbum(int photo_id,int album_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("INSERT INTO links (id_photos,id_albums) VALUES (:id_photos,:id_albums);");
    query.bindValue(":id_photos",photo_id);
    query.bindValue(":id_albums", album_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}
bool SqliteStorage::removePhotoAlbum(int photo_id,int album_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE id_photos=:id_photos AND id_albums=:id_albums;");
    query.bindValue(":id_photos",photo_id);
    query.bindValue(":id_albums", album_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}
