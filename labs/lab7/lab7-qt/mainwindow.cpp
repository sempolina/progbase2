#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "adddialog.h"
#include "edditdialog.h"
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pix("../camera.png");
    ui->label->setPixmap(pix);
    connect(ui->actionNew,&QAction::triggered,this,&MainWindow::onNew);
    connect(ui->actionOpen,&QAction::triggered,this,&MainWindow::onOpen);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::beforeClose);
}

MainWindow::~MainWindow()
{
    if(storage_!=nullptr)
        delete storage_;
    delete ui;
}
void MainWindow::addPhotos(const vector<Photo> & photos)
{
    for(const Photo & photo:photos)
    {
        QVariant qVar=QVariant::fromValue(photo);
        QListWidgetItem * qPhotoListItem=new QListWidgetItem();
        qPhotoListItem->setText(QString::fromStdString(photo.User));
        qPhotoListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qPhotoListItem);
    }
}
void MainWindow::onNew()
{
    ui->add_button->setEnabled(true);
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir=QDir::currentPath();
    QString default_name="new_storage";
    QString folder_path =dialog.getSaveFileName(this,"Select New Storage Folder",current_dir+"/"+default_name,"Folders");
    qDebug()<<folder_path;
    if(storage_!=nullptr)
    {
        delete storage_;
        ui->listWidget->clear();
    }
    storage_= new XmlStorage(folder_path.toStdString());
    this->addPhotos(storage_->getAllPhotos());
}
void MainWindow::onOpen()
{
    ui->add_button->setEnabled(true);
   QString fileName=QFileDialog::getOpenFileName(this,"Dialog Caption","","XML (*.xml)");
   qDebug()<<fileName;
   if(storage_!=nullptr)
   {
       delete storage_;
       ui->listWidget->clear();
   }
   storage_=new XmlStorage(fileName.toStdString());
   vector<Photo> photos=storage_->getAllPhotos();
   this->addPhotos(photos);
}


void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
        ui->remove_button->setEnabled(true);
        ui->edit_button->setEnabled(true);
    Photo ph= item->data(Qt::UserRole).value<Photo>();
    ui->selected->setText("Selected Photo:\nId:"+QString::number(ph.id)+
                          "\nUser:"+ QString::fromStdString(ph.User)+
                          "\nlikes:"+QString::number(ph.likes)+"\ncomments:"+QString::number(ph.comments));
    qDebug()<<ph.comments;
}

void MainWindow::on_remove_button_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        if(ui->listWidget->selectedItems().isEmpty())
        {
            ui->selected->hide();
           ui->remove_button->setEnabled(false);
           ui->edit_button->setEnabled(false);
        }
        QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
        Photo ph;
        foreach(QListWidgetItem *item,items)
        {
            ph=item->data(Qt::UserRole).value<Photo>();
            storage_->removePhoto(ph.id);
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        }
    }
}

void MainWindow::on_add_button_clicked()
{
    AddDialog add(this);
    int status=add.exec();
    if(status==1)
    {
        Photo photo=add.data();
        photo.id=storage_->insertPhoto(photo);
        QVariant qVar=QVariant::fromValue(photo);
        QListWidgetItem * qPhotoListItem=new QListWidgetItem();
        qPhotoListItem->setText(QString::fromStdString(photo.User));
        qPhotoListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qPhotoListItem);
    }
    else
    {

    }
}

void MainWindow::on_edit_button_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Photo ph;
        foreach(QListWidgetItem *item,items)
        {
            ph=item->data(Qt::UserRole).value<Photo>();
            EdditDialog edit(this);
            edit.setData(ph);
            int status=edit.exec();
            if(status==1)
            {
                Photo photo=edit.data();
                photo.id=ph.id;
                storage_->updatePhoto(photo);
                QVariant qVar=QVariant::fromValue(photo);
                item->setData(Qt::UserRole,qVar);
                item->setText(QString::fromStdString(photo.User));
                ui->listWidget->editItem(item);
                ui->selected->setText("Selected Photo:\nId:"+QString::number(photo.id)+
                                      "\nUser:"+ QString::fromStdString(photo.User)+
                                      "\nlikes:"+QString::number(photo.likes)+"\ncomments:"+QString::number(photo.comments));
            }

        }
}
void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On exit","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        this->close();
    }
}
