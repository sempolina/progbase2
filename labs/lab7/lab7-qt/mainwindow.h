#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <xmlstorage.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addPhotos(const vector<Photo> & photos);
private slots:
    void onNew();
    void onOpen();


    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_remove_button_clicked();

    void on_add_button_clicked();

    void on_edit_button_clicked();
    void beforeClose();
private:
    Ui::MainWindow *ui;
    FileStorage * storage_=nullptr;
};

#endif // MAINWINDOW_H
