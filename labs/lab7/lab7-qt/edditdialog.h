#ifndef EDDITDIALOG_H
#define EDDITDIALOG_H

#include <QDialog>
#include"photo.h"
namespace Ui {
class EdditDialog;
}

class EdditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EdditDialog(QWidget *parent = 0);
    ~EdditDialog();
     Photo data();
     void setData(const Photo & ph);
private:
    Ui::EdditDialog *ui;
};

#endif // EDDITDIALOG_H
