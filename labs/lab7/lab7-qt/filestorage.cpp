#include "filestorage.h"
#include <QDir>
#include <QDebug>
using namespace std;

bool FileStorage::open()
{
    int ind =dir_name_.rfind("/");
   if(dir_name_.find(".xml")!=string::npos)
   {
       photo_=dir_name_.substr(ind+1);
       dir_name_.erase(ind+1);
         qDebug()<<QString::fromStdString(dir_name_)<<"|"<<QString::fromStdString(photo_);
   }
   else if(!QDir(QString::fromStdString(dir_name_+"/")).exists())
   {
       QDir().mkdir(QString::fromStdString(dir_name_));
       dir_name_.push_back('/');
       vector<Photo> ph;
         ofstream fin(dir_name_+"photos_id.csv");
         fin<<"0";
         fin.close();
       this->savePhotos(ph);
   }
   photos_file_.open(dir_name_+photo_);
   return photos_file_.is_open();
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name()
{
    return dir_name_;
}
bool FileStorage::isOpen()
{

    return photos_file_.is_open();
}
void FileStorage::close()
{
    photos_file_.close();

}
vector<Photo> FileStorage::getAllPhotos()
{
    vector<Photo> photos=this->loadPhotos();
    return photos;
}
optional<Photo> FileStorage::getPhotoById(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo_id)
            return photos[i];
    }
    return nullopt;
}
bool FileStorage::updatePhoto(const Photo &photo)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo.id)
        {
            photos[i] = photo;
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
bool FileStorage::removePhoto(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    auto it = photos.begin();
    auto it_end = photos.end();
    for (; it != it_end; it++)
    {
        if ((*it).id == photo_id)
        {
            photos.erase(it);
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
int FileStorage::insertPhoto(const Photo &photo)
{
    int newId = this->getNewPhotoId();
    Photo copy = photo;
    copy.id = newId;
    vector<Photo> photos = this->loadPhotos();
    photos.push_back(copy);
    this->savePhotos(photos);
    return newId;
}
