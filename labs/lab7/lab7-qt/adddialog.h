#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include"photo.h"
using namespace std;
namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();
    Photo data();
private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H
