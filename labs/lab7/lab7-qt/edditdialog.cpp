#include "edditdialog.h"
#include "ui_edditdialog.h"
EdditDialog::EdditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EdditDialog)
{
    ui->setupUi(this);
}
void EdditDialog::setData(const Photo & ph)
{
    ui->lineEdit->setText(QString::fromStdString( ph.User));
    ui->spinBox->setValue(ph.likes);
    ui->spinBox_2->setValue(ph.comments);
}
Photo EdditDialog::data()
{
    Photo ph;
    ph.User=ui->lineEdit->text().toStdString();
    ph.likes=ui->spinBox->value();
    ph.comments=ui->spinBox_2->value();
    return ph;
}
EdditDialog::~EdditDialog()
{
    delete ui;
}
