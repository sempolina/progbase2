#include "xmlstorage.h"
#include <QDebug>
using namespace std;
static Photo xmlElementToPhoto(QDomElement & photoE1)
{
    Photo photo;
    photo.id=photoE1.attribute("id").toInt();
    photo.User=photoE1.attribute("User").toStdString();
    photo.likes=photoE1.attribute("Likes").toInt();
    photo.comments=photoE1.attribute("Comments").toInt();
    return photo;
}
vector<Photo> XmlStorage::loadPhotos()
{
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(photos_file_, str2);
    while (!photos_file_.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(photos_file_, str2);
    }
    str.append(str2);
    QDomDocument doc;
    QString errorMessage;
    QString xmlstr=QString::fromStdString(str);
    if(!doc.setContent(xmlstr,&errorMessage))
    {
        qDebug()<<"error parsing xml: "<<errorMessage;
        exit(1);
    }
    QDomElement rootE1=doc.documentElement();
    QDomNodeList rootChildren=rootE1.childNodes();
      vector<Photo> pho;
    for(int i=0;i<rootChildren.length();i++)
    {
        QDomNode photoNode=rootChildren.at(i);
        QDomElement photoE1=photoNode.toElement();
        Photo photo = xmlElementToPhoto(photoE1);

        pho.push_back(photo);

    }
    this->close();
    return pho;
}
static QDomElement photoToXmlElement(QDomDocument & doc,const Photo & photo)
{
 QDomElement photoE1=doc.createElement("Photo");
 photoE1.setAttribute("id",photo.id);
 photoE1.setAttribute("User",QString::fromStdString(photo.User));
 photoE1.setAttribute("Likes",photo.likes);
 photoE1.setAttribute("Comments",photo.comments);
 return photoE1;
}
void XmlStorage::savePhotos(const vector<Photo> & photos)
{
    QDomDocument doc;
    QDomElement rootEl =doc.createElement("Photos");
    for(int i=0;i<photos.size();i++)
    {
        QDomElement photoE1=photoToXmlElement(doc,photos[i]);
        rootEl.appendChild(photoE1);
    }
    doc.appendChild(rootEl);
    QString qStr =doc.toString(4);
    photos_file_.open(this->name()+photo_,ios::out);
    if (!photos_file_.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    photos_file_ << qStr.toStdString();
    photos_file_.close();
}
int XmlStorage::getNewPhotoId()
{
    fstream id(this->name()+ "photos_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(this->name() + "photos_id.csv", ios::out);
    if (m == 0)
    {
        vector<Photo> ph = this->loadPhotos();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}

