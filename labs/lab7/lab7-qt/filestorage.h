#ifndef FILESTORAGE_H
#define FILESTORAGE_H


#include <iostream>
#include<fstream>
#include<vector>
#include <optional>
#include "photo.h"
#include <string>
using namespace std;

class FileStorage
{
    string dir_name_;
protected:
    fstream photos_file_;
    string photo_="";
    virtual vector<Photo> loadPhotos()=0;
    virtual void savePhotos(const vector<Photo> & photos)=0;
    virtual int getNewPhotoId()=0;
    public:
    FileStorage(const string & dir_name=""){dir_name_ = dir_name;}
    virtual ~FileStorage(){}
     void setName(const string & dir_name);
     string name();
     bool isOpen();
    bool open();
     void close();

     vector<Photo> getAllPhotos();
     optional<Photo> getPhotoById(int photo_id);
     bool updatePhoto(const Photo & photo);
     bool removePhoto(int photo_id);
     int insertPhoto(const Photo & photo);

};
//Q_DECLARE_METATYPE(FileStorage);
#endif // FILESTORAGE_H
