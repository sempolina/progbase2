#pragma once
#ifndef PHOTO_H
#define PHOTO_H

#endif // PHOTO_H

#include<string>
#include <iostream>
#include <QListWidgetItem>
using namespace std;
struct Photo
{
    int id;
    string User;
    int likes;
    int comments;
    Photo(){}
    Photo(const Photo & al){this->id=al.id;this->likes=al.likes;this->User=al.User;this->comments=al.comments;}
};
Q_DECLARE_METATYPE(Photo);
