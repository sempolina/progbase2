#pragma once 
#include <cstdlib>
#include <iostream>
#include <string>
#include "photo.h"
using namespace std;
class DynamicArray
{
    Photo *items_;
    int capacity_;

public:
    DynamicArray(size_t size);
    ~DynamicArray();
    size_t size();
    void resize(size_t newSize);
    Photo get(int index);
    void set(int index, Photo value);
};