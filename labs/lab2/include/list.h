#pragma once
#include "dyn_arr.h"
class List
{
    DynamicArray array_;
    size_t size_;

public:
    List();
    ~List();
    size_t size();
    Photo get(int index);
    void set(int index, Photo value);
    void remove_at(int index);
    void push_back(Photo value);
    // void push_front(float value);
    bool empty();
    void print();
};