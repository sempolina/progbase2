#include <iostream>
#include <string>
#include <fstream>
#include "csv.h"
#include "list.h"
#include "fs.h"
using namespace std;
List createEntityListFromTable(StringTable &csvTable)
{
    List ls;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        Photo ph;
        ph.id = atoi(csvTable.at(i, 0).c_str());
        ph.User = csvTable.at(i, 1);
        ph.likes = atoi(csvTable.at(i, 2).c_str());
        ph.comments = atoi(csvTable.at(i, 3).c_str());
        ls.push_back(ph);
    }
    return ls;
}
void processEntities(List &items, int n)
{
    for (int i = 0; i < items.size(); i++)
    {
        if (items.get(i).likes >= n)
        {
            items.remove_at(i);
            i--;
        }
    }
}
StringTable createTableFromEntityList(List &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) ="id";
    st.at(0, 1) ="User";
    st.at(0, 2) ="Likes";
    st.at(0, 3) ="Comments";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = to_string(ls.get(i).id);
        st.at(i + 1, 1) = ls.get(i).User;
        st.at(i + 1, 2) = to_string(ls.get(i).likes);
        st.at(i + 1, 3) = to_string(ls.get(i).comments);
    }
    return st;
}
int main()
{
    string str=infs("../data.csv");
    StringTable inCsvTable = Csv_parse(str);
    cout << "StringTable:" << endl;
    inCsvTable.print();
    List items = createEntityListFromTable(inCsvTable);
    cout << "List:" << endl;
    items.print();
    cout << "Please,enter n:";
    int n;
    cin >> n;
    processEntities(items, n);
    cout << endl<< "List:" << endl;
    items.print();
    StringTable outCsvTable=createTableFromEntityList(items);
    cout << "StringTable:" << endl;
    outCsvTable.print();
    string strout=Csv_toString(outCsvTable);
    cout<<strout;
    outfs("../out.csv",strout);
}
