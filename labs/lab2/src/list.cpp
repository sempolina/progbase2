#include <iostream>
#include <stdlib.h>
#include <math.h>
#include "list.h"
using namespace std;
List::List() : array_{5}
{
    size_ = 0;
}
List::~List()
{
    
}
size_t List::size()
{
    return size_;
}
Photo List::get(int index)
{
    return array_.get(index);
}
void List::set(int index, Photo value)
{
    array_.set(index, value);
}
void List::remove_at(int index)
{
    size_--;
    for (int i = index; i < size_; i++)
    {
        array_.set(i, array_.get(i + 1));
    }
}
void List::push_back(Photo value)
{
    if (size_ == array_.size())
    {
        array_.resize(size_ + 10);
    }
    array_.set(size_, value);
    size_++;
}
// void List::push_front(float value)
// {
//     if (size_ == array_.size())
//     {
//         array_.resize(array_.size + 10);
//     }
//     for (int i = size_; i > 0; i--)
//     {
//         array_.set(i, array_.get(i - 1));
//     }
//     size_++;
//     array_.set(0, value);
// }
bool List::empty()
{
    if (size_ == 0)
        return true;
    else
        return false;
}
void List::print()
{
    cout<<"Id,User,Likes,Comments"<<endl;
    for(int i=0;i<size_;i++)
    {
    Photo ph=array_.get(i);
    cout<<ph.id <<","<<ph.User<<","<<ph.likes<<","<<ph.comments<<endl;
    }
    cout<<endl;
}