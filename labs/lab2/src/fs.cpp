#include "fs.h"
using namespace std;
string infs(const char * namefs)
{
    ifstream fin(namefs);
    if (!fin.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str;
    string str2;
    getline(fin, str2);
    while (!fin.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(fin, str2);
    }
    str.append(str2);
    str.push_back('\n');
    fin.close();
    return str;
};
void outfs(const char *namefs, string strout)
{
    ofstream fout(namefs);
    fout << strout;
    fout.close();
}