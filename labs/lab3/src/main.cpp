#include <string>
#include <fstream>
#include "photo.h"
#include "csv.h"
#include "list.h"
#include "options.h"
#include "fs.h"
#include "BSTree.h"
using namespace std;
List<Photo> createEntityListFromTable(StringTable &csvTable)
{
    List<Photo> ls;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        Photo ph;
        ph.id = atoi(csvTable.at(i, 0).c_str());
        ph.User = csvTable.at(i, 1);
        ph.likes = atoi(csvTable.at(i, 2).c_str());
        ph.comments = atoi(csvTable.at(i, 3).c_str());
        ls.push_back(ph);
    }
    return ls;
}
void processEntities(List<Photo> &items, int n, BSTree *bst = nullptr)
{
    for (int i = 0; i < items.size(); i++)
    {
        if (items.get(i).likes >= n)
        {
            if (bst != nullptr)
            {
                bst->remove(items.get(i).id);
            }
            items.remove_at(i);
            i--;
        }
    }
}
StringTable createTableFromEntityList(List<Photo> &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) = "id";
    st.at(0, 1) = "User";
    st.at(0, 2) = "Likes";
    st.at(0, 3) = "Comments";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = to_string(ls.get(i).id);
        st.at(i + 1, 1) = ls.get(i).User;
        st.at(i + 1, 2) = to_string(ls.get(i).likes);
        st.at(i + 1, 3) = to_string(ls.get(i).comments);
    }
    return st;
}
int main(int argc, char *argv[])
{
    Options opt = getProgramOptions(argc, argv);
    string str = infs(opt.input_file_name);
    StringTable inCsvTable = Csv_parse(str);
    List<Photo> items = createEntityListFromTable(inCsvTable);
    cout << "List:" << endl;
    items.print();
    if (opt.build_bstree)
    {
        BSTree bst;
        for (int i = 0; i < items.size(); i++)
            bst.insert(items.get(i));
        bst.print();
        cout<<endl;
        if (opt.n_process != -1)
        {
            processEntities(items, opt.n_process, &bst);
            cout << "List:" << endl;
            items.print();
            bst.print();
        }
    }
    else
    {
        if (opt.n_process != -1)
        {
            processEntities(items, opt.n_process);
            cout << "List:" << endl;
            items.print();
        }
    }
    StringTable outCsvTable = createTableFromEntityList(items);
    string strout = Csv_toString(outCsvTable);
    if (opt.output_file_name != "")
        outfs(opt.output_file_name, strout);
}