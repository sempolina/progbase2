#include "options.h"
using namespace std;
Options getProgramOptions(int argc, char *argv[])
{
    Options opt;
    int k = 1;
    while (k < argc)
    {
        if (argv[k][0] != '-')
            opt.input_file_name=argv[k];
        else
        {
            k++;
            if (argv[k - 1][1] == 'o')
                opt.output_file_name=argv[k];
            else if (argv[k - 1][1] == 'b')
            {
                k--;
                opt.build_bstree = true;
            }
            else
                opt.n_process = atoi(argv[k]);
        }
        k++;
    }
    return opt;
}