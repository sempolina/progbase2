#include "BSTree.h"
using namespace std;
BSTree::BSTree()
{
}
Photo BSTree::removeNode(BinTree *node, int key, BinTree *parent)
{
    if (node->key_ == key)
    {
        int k = node->key_;
        BinTree *right = node->right;
        BinTree *left = node->left;
        if (parent == nullptr)
        {
            if (right == nullptr)
                root = left;
            else
            {
                BinTree *l = right->left;
                if (l == nullptr)
                {
                    root = right;
                    right->left = left;
                }
                else
                {
                    BinTree *p = right;
                    while (l->left != nullptr)
                    {
                        p = l;
                        l = l->left;
                    }
                    p->left = l->right;
                    root = l;
                    l->right = right;
                    while (l->left != nullptr)
                        l = l->left;
                    l->left = left;
                }
            }
        }
        else
        {
            if (right == nullptr)
            {
                if (parent->left != nullptr)
                {
                    if (parent->left->key_ == k)
                        parent->left = left;
                    else
                        parent->right = left;
                }
                else
                    parent->right = left;
            }
            else
            {
                BinTree *l = right->left;
                if (l == nullptr)
                {
                    if (parent->left != nullptr)
                    {
                        if (parent->left->key_ == k)
                            parent->left = right;
                        else
                            parent->right = right;
                    }
                    else
                        parent->right = right;
                        right->left=left;
                }
                else
                {
                    BinTree *p = right;
                    while (l->left != nullptr)
                    {
                        p = l;
                        l = l->left;
                    }
                    p->left = l->right;
                    if (parent->left != nullptr)
                    {
                        if (parent->left->key_ == k)
                            parent->left = l;
                        else
                            parent->right = l;
                    }
                    else
                        parent->right = l;
                    l->right = right;
                    l->left = left;
                }
            }
        }
        Photo pt = node->value;
        delete node;
        return pt;
    }
    if (node->key_ < key)
        return this->removeNode(node->right, key, node);
    if (node->key_ > key)
        return this->removeNode(node->left, key, node);
}
Photo BSTree::remove(int key)
{
    if (this->lookup(key) == false)
    {
        fprintf(stderr, "There no this key");
        exit(1);
    }

    return this->removeNode(root, key, nullptr);
}
void BSTree::clear()
{
    while (root != nullptr)
        this->remove(root->key_);
}
BSTree::~BSTree()
{
    this->clear();
}
size_t BSTree::size()
{
    return size_;
}
static void insertNode(BinTree *node, BinTree *newN)
{
    if (newN->key_ > node->key_)
    {
        if (node->right == nullptr)
            node->right = newN;
        else
            insertNode(node->right, newN);
    }
    else if (newN->key_ < node->key_)
    {
        if (node->left == nullptr)
            node->left = newN;
        else
            insertNode(node->left, newN);
    }
}
void BSTree::insert(Photo value)
{
    int k = value.id;
    BinTree *node = new BinTree{k, value};
    size_++;
    if (root == nullptr)
        root = node;
    else
        insertNode(root, node);
}
static bool lookupNode(BinTree *node, int key)
{
    if (node->key_ == key)
        return true;
    else
    {
        if (key > node->key_)
        {
            if (node->right == nullptr)
                return false;
            else
                return lookupNode(node->right, key);
        }
        else if (key < node->key_)
        {
            if (node->left == nullptr)
                return false;
            else
                return lookupNode(node->left, key);
        }
    }
}
bool BSTree::lookup(int key)
{
    if (root == nullptr)
        return false;
    else if (root->key_ == key)
        return true;
    else
    {
        if (key > root->key_)
        {
            if (root->right == nullptr)
                return false;
            else
                return lookupNode(root->right, key);
        }
        else if (key < root->key_)
        {
            if (root->left == nullptr)
                return false;
            else
                return lookupNode(root->left, key);
        }
    }
}
static Photo searchNode(BinTree *node, int key)
{
    if (node->key_ == key)
        return node->value;
    else
    {
        if (key > node->key_)
            return searchNode(node->right, key);
        else if (key < node->key_)
            return searchNode(node->left, key);
    }
}
Photo BSTree::search(int key)
{
    if (this->lookup(key) == false)
    {
        fprintf(stderr, "There no this key");
        exit(1);
    }
    if (root->key_ == key)
        return root->value;
    else
    {
        if (key > root->key_)
            return searchNode(root->right, key);
        else if (key < root->key_)
            return searchNode(root->left, key);
    }
}
static void printN(BinTree *node, int level)
{
    if (node->left != nullptr)
    {
        for (int i = 0; i < level; i++)
            cout << ".";
        cout << "l:" << node->left->value.id << " " << node->left->value.User << " " << node->left->value.likes << " " << node->left->value.comments << endl;
        printN(node->left, level + 1);
    }
    if (node->right != nullptr)
    {
        for (int i = 0; i < level; i++)
            cout << ".";
        cout << "r:" << node->right->value.id << " " << node->right->value.User << " " << node->right->value.likes << " " << node->right->value.comments << endl;
        printN(node->right, level + 1);
    }
}
void BSTree::print()
{
    cout << "BSTree" << endl;
    if (root != nullptr)
    {
        cout << "root:" << root->value.id << " " << root->value.User << " " << root->value.likes << " " << root->value.comments << endl;
        printN(root, 1);
    }
}
