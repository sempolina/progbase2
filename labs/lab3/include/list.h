#pragma once
#include <iostream>
#include <stdlib.h>
#include "dyn_arr.h"
using namespace std;
template <typename T>
class List
{
    DynamicArray<T> array_;
    size_t size_;

public:
    List() : array_{5}
    {
        size_ = 0;
    }
    ~List() {}
    size_t size()
    {
        return size_;
    }
    T get(int index)
    {
        return array_.get(index);
    }
    void set(int index, T value)
    {
        array_.set(index, value);
    }
    void remove_at(int index)
    {
        size_--;
        for (int i = index; i < size_; i++)
        {
            array_.set(i, array_.get(i + 1));
        }
    }
    void push_back(T value)
    {
        if (size_ == array_.size())
        {
            array_.resize(size_ + 10);
        }
        array_.set(size_, value);
        size_++;
    }
    bool empty()
    {
        if (size_ == 0)
            return true;
        else
            return false;
    }
    void print()
    {
        cout << "Id,User,Likes,Comments" << endl;
        for (int i = 0; i < size_; i++)
        {
            T ph = array_.get(i);
            cout << ph.id << "," << ph.User << "," << ph.likes << "," << ph.comments << endl;
        }
        cout << endl;
    }
};