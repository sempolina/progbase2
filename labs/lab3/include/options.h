#pragma once
#include <iostream>
#include<string>
using namespace std;
struct Options
{
    string input_file_name="";
    int n_process=-1;
    string output_file_name="";
    bool build_bstree=false;
};
Options getProgramOptions(int argc,char* argv[]);