#pragma once
#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;
template <typename T>
class DynamicArray
{
    T *items_;
    int capacity_;

public:
    DynamicArray(size_t initial_cap)
    {
        items_ = new T[initial_cap];
        capacity_ = initial_cap;
    }
    ~DynamicArray()
    {
        delete[] items_;
    }
    size_t size()
    {
        return capacity_;
    }
    void resize(size_t newSize)
    {
        T *n = new T[newSize];
        int min;
        if (newSize < capacity_)
            min = newSize;
        else
            min = capacity_;
        for (int i = 0; i < min; i++)
        {
            n[i] = items_[i];
        }
        delete[] items_;
        items_ = n;
        capacity_ = newSize;
    }
    T get(int index)
    {
        return items_[index];
    }
    void set(int index, T value)
    {
        items_[index] = value;
    }
};
