#pragma once
#include <iostream>
#include "photo.h"
struct BinTree
{
    int key_;
    Photo value;
    BinTree *left=nullptr;
    BinTree *right=nullptr;
    explicit BinTree(int k,Photo v):key_{k},value{v} {}
};