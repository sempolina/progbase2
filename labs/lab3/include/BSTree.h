#include "BinTree.h"
using namespace std;
class BSTree
{
    BinTree *root=nullptr;
    size_t size_=0;
    public:
    BSTree();
    ~BSTree();
    size_t size();
    void insert(Photo value);
    bool lookup(int key);
    Photo search(int key);
    Photo removeNode(BinTree *node, int key, BinTree *parent);
    Photo remove(int key);
    void clear();
    void print();
};