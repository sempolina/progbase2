#include <string>
#include <vector>
#include "photo.h"
#include "csv.h"
#include "storage.h"
#include "cui.h"
using namespace std;
int main()
{
    cout<<"Please, enter name of directory"<<endl;
    string str;
    cin>>str;
    FileStorage fs{str};
    Cui cui{&fs};
    cui.show();
}