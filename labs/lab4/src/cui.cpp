#include "cui.h"
#include <vector>
using namespace std;
void Cui::PhotosMainMenu()
{
    vector<Photo> ph = storage_->getAllPhotos();
    cout << "Id,User" << endl;
    for (int i = 0; i < ph.size(); i++)
        cout << ph[i].id << "," << ph[i].User << endl;
    cout << endl;
    this->show();
}
void Cui::PhotoMenu(int photo_id)
{
    optional<Photo> ph = storage_->getPhotoById(photo_id);
    if (!ph)
        cout << "Incorrect id\n";
    else
    {
        cout << "Id,User,likes,comments" << endl;
        cout << ph.value().id << "," << ph.value().User << "," << ph.value().likes << "," << ph.value().comments << endl;
    }
    this->show();
}
void Cui::photoUpdateMenu(int photo_id)
{
    optional<Photo> photo = storage_->getPhotoById(photo_id);
    if (!photo)
        cout << "Incorrect id\n";
    else
    {
        Photo ph = photo.value();
        cout << "Please, choose, what you want to change:\n1. User\n2. likes\n3. comments" << endl;
        int com;
        cin >> com;
        while (com > 3 || com < 1)
        {
            cout << "Incorrect command, try again\n";
            cin >> com;
        }
        if (com == 1)
        {
            cout << "Enter new User" << endl;
            cin >> ph.User;
        }
        if (com == 2)
        {
            cout << "Enter new likes" << endl;
            cin >> ph.likes;
        }
        if (com == 3)
        {
            cout << "Enter new comments" << endl;
            cin >> ph.comments;
        }
        storage_->updatePhoto(ph);
    }
    this->show();
}
void Cui::photoDeleteMenu(int photo_id)
{
    if (!storage_->removePhoto(photo_id))
        cout << "Incorrect id" << endl;
    this->show();
}
void Cui::photoCreateMenu()
{
    Photo ph;
    cout << "Please, enter User" << endl;
    cin >> ph.User;
    cout << "Please, enter likes" << endl;
    cin >> ph.likes;
    cout << "Please, enter comments" << endl;
    cin >> ph.comments;
    cout << "New Id:" << storage_->insertPhoto(ph) << endl;
    this->show();
}
void Cui::show()
{
    cout << "Please, enter option:\n1. Show all photos\n2. Choose photo by id\n3. Add new photo\n4. Finish programm\n";
    int com = 0;
    cin >> com;
    while (com > 4 || com < 1)
    {
        cout << "Incorrect command, try again\n";
        cin >> com;
    }
    if (com == 4)
        return;
    if (com == 1)
        this->PhotosMainMenu();
    if (com == 2)
    {
        cout << "Please, enter id\n";
        int id;
        cin >> id;
        cout << "Please, enter option with photo by id:\n1. Show more information about element\n2. Change a field\n3. Delete photo\n";
        cin >> com;
        while (com > 3 || com < 1)
        {
            cout << "Incorrect command, try again\n";
            cin >> com;
        }
        if (com == 1)
            this->PhotoMenu(id);
        if (com == 2)
            this->photoUpdateMenu(id);
        if (com == 3)
            this->photoDeleteMenu(id);
    }
    if (com == 3)
    {
        this->photoCreateMenu();
    }
}
