#include <string>
#include "storage.h"
#include "csv.h"
#include "string_table.h"
using namespace std;
static vector<Photo> createEntityListFromTable(StringTable &csvTable)
{
    vector<Photo> ls;
    for (int i = 1; i < csvTable.size_rows(); i++)
    {
        Photo ph;
        ph.id = atoi(csvTable.at(i, 0).c_str());
        ph.User = csvTable.at(i, 1);
        ph.likes = atoi(csvTable.at(i, 2).c_str());
        ph.comments = atoi(csvTable.at(i, 3).c_str());
        ls.push_back(ph);
    }
    return ls;
}
vector<Photo> FileStorage::loadPhotos()
{
    if (!this->open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    string str2;
    string str;
    getline(photos_file_, str2);
    while (!photos_file_.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(photos_file_, str2);
    }
    str.append(str2);
    str.push_back('\n');
    this->close();
    StringTable inCsvTable = Csv_parse(str);
    vector<Photo> items = createEntityListFromTable(inCsvTable);
    return items;
}
static StringTable createTableFromEntityList(vector<Photo> &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) = "id";
    st.at(0, 1) = "User";
    st.at(0, 2) = "Likes";
    st.at(0, 3) = "Comments";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = to_string(ls[i].id);
        st.at(i + 1, 1) = ls[i].User;
        st.at(i + 1, 2) = to_string(ls[i].likes);
        st.at(i + 1, 3) = to_string(ls[i].comments);
    }
    return st;
}
void FileStorage::savePhotos(const vector<Photo> &photos)
{
    vector<Photo> ph = photos;
    StringTable outCsvTable = createTableFromEntityList(ph);
    string strout = Csv_toString(outCsvTable);
    if (!this->open(true))
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    photos_file_ << strout;
    this->close();
}
int FileStorage::getNewPhotoId()
{
    fstream id(dir_name_ + "photos_id.csv");
    if (!id.is_open())
    {
        fprintf(stderr, "File can`t be open");
        exit(1);
    }
    int m;
    id >> m;
    id.close();
    id.open(dir_name_ + "photos_id.csv",ios::out);
    if (m == 0)
    {
        vector<Photo> ph = this->loadPhotos();
        for (int i = 0; i < ph.size(); i++)
        {
            if (m < ph[i].id)
                m = ph[i].id;
        }
    }
    id << m + 1;
    id.close();
    return m + 1;
}
FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name()
{
    return dir_name_;
}
bool FileStorage::isOpen()
{
    return photos_file_.is_open();
}
bool FileStorage::open(bool issave)
{
    if (issave)
        photos_file_.open(dir_name_ + "photos.csv", ios::out);
    else
        photos_file_.open(dir_name_ + "photos.csv");
    return photos_file_.is_open();
}
void FileStorage::close()
{
    photos_file_.close();
}
vector<Photo> FileStorage::getAllPhotos()
{
    return this->loadPhotos();
}
optional<Photo> FileStorage::getPhotoById(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo_id)
            return photos[i];
    }
    return nullopt;
}
bool FileStorage::updatePhoto(const Photo &photo)
{
    vector<Photo> photos = this->loadPhotos();
    for (int i = 0; i < photos.size(); i++)
    {
        if (photos[i].id == photo.id)
        {
            photos[i] = photo;
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
bool FileStorage::removePhoto(int photo_id)
{
    vector<Photo> photos = this->loadPhotos();
    auto it = photos.begin();
    auto it_end = photos.end();
    for (; it != it_end; it++)
    {
        if ((*it).id == photo_id)
        {
            photos.erase(it);
            this->savePhotos(photos);
            return true;
        }
    }
    return false;
}
int FileStorage::insertPhoto(const Photo &photo)
{
    int newId = this->getNewPhotoId();
    Photo copy = photo;
    copy.id = newId;
    vector<Photo> photos = this->loadPhotos();
    photos.push_back(copy);
    this->savePhotos(photos);
    return newId;
}