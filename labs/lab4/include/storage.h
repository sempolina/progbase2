#pragma once
#include <iostream>
#include<fstream>
#include<vector>
#include <optional>
#include "photo.h"

using namespace std;

class FileStorage
{
    string dir_name_;
    fstream photos_file_;
    vector<Photo> loadPhotos();
    void savePhotos(const vector<Photo> & photos);
    int getNewPhotoId();
    public:
     explicit FileStorage(const string & dir_name="");
     void setName(const string & dir_name);
     string name();
     bool isOpen();
     bool open(bool issave = false);
     void close();
     vector<Photo> getAllPhotos();
     optional<Photo> getPhotoById(int photo_id);
     bool updatePhoto(const Photo & photo);
     bool removePhoto(int photo_id);
     int insertPhoto(const Photo & photo);
};